#!/bin/sh
rm -R deploy/
mkdir -p deploy/src
cp -r icons deploy/
cp manifest.json deploy/
cp src/popup.html src/popup.css deploy/src/
java -jar scripts/minifier.jar --js src/jquery.js --js_output_file deploy/src/jquery.js --warning_level QUIET
java -jar scripts/minifier.jar --js src/ga.js --js_output_file deploy/src/ga.js --warning_level QUIET
java -jar scripts/minifier.jar --js src/constants.js --js_output_file deploy/src/constants.js --warning_level QUIET
java -jar scripts/minifier.jar --js src/analytics.js --js_output_file deploy/src/analytics.js --warning_level QUIET
java -jar scripts/minifier.jar --js src/parser.js --js_output_file deploy/src/parser.js --warning_level QUIET
java -jar scripts/minifier.jar --js src/panel.js --js_output_file deploy/src/panel.js --warning_level QUIET
java -jar scripts/minifier.jar --js src/background.js --js_output_file deploy/src/background.js --warning_level QUIET
java -jar scripts/minifier.jar --js src/popup.js --js_output_file deploy/src/popup.js --warning_level QUIET
