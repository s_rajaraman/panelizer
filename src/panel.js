/**
 * Created by hrajaram on 7/23/14.
 */
var panel = {
    create: function (url) {
        chrome.windows.create({
            url: url,
            height: 400,
            width: 500,
            type: "panel"
        });
    }
};