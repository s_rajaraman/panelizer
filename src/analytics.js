/**
 * Created by hrajaram on 7/23/14.
 */
var analytics = {
    track: function (category, action, label) {
        _gaq.push(['_trackEvent', category, action, label]);
    }
};

_gaq.push(['_setAccount', constants.analytics.id]);
_gaq.push(['_trackPageview']);
