/**
 * Created by hrajaram on 7/23/14.
 */
var constants = {
    analytics: {
        id: 'UA-53151442-1',
        interval: 1000,
        categories: {
            background: 'background',
            pageAction: 'pageAction',
            donate: 'donate'
        },
        actions: {
            background: {
                link: 'link',
                selection: 'selection'
            },
            pageAction: {
                submit: 'submit',
                enter: 'enter'
            }
        }
    }
};
