/**
 * Created by Sriram on 7/22/14.
 */
$(document).ready(function () {
    var $donateButton = $('#donate_button'),
        $searchBar = $('#searchBar'),
        $submitButton = $('#submitButton'),

        DONATE_BUTTON_CLICK_KEY = 'dbc',
        NUMBER_OF_USES_KEY = 'NUMBER_OF_USES',
        donateButtonClicked = localStorage[DONATE_BUTTON_CLICK_KEY] || false,
        numberofUses = localStorage[NUMBER_OF_USES_KEY] || 1;

        if(!localStorage[NUMBER_OF_USES_KEY]){
            localStorage[NUMBER_OF_USES_KEY] = 1;
        }

        onSubmitClicked = function (trkAction) {
            var searchBarValue = $searchBar.val();
            var url = searchBarValue && searchBarValue.length > 0 && parser.parseUrl(searchBarValue);

            analytics.track(constants.analytics.categories.pageAction, trkAction, url);
            panel.create(url)
        };

    $submitButton.click(function() {
        onSubmitClicked(constants.analytics.actions.pageAction.submit)
    });

    $searchBar.keyup(function (e) {
        if (e.keyCode == 13) {
            onSubmitClicked(constants.analytics.actions.pageAction.enter);
        }
    });


    if (donateButtonClicked || numberofUses % 20 != 0) {
        $donateButton.hide();
    }

    $donateButton.click(function () {
        analytics.track(constants.analytics.categories.donate, 'donateAction', 'donateLabel');
        localStorage[DONATE_BUTTON_CLICK_KEY] = true;
        $(this).hide();
    });

     localStorage[NUMBER_OF_USES_KEY] = parseInt(numberofUses) + 1;

});