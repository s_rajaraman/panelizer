var parser = {
    /**
     * Sometimes the urls may not be fully formed, example www.google.com?search=something
     * In this case the url sent out would be chrome-extension://<id>/src/www.google.com?search=something
     *
     * This function parses it correctly and creates a fully formed url that starts with http:
     * @param url
     * @returns {string}
     */

    parseUrl: function (url) {
        var HTTP = 'http',
            CHROME = 'chrome',
            PATH = '/src';
        //^^this is the path where all of our code runs we need to remove this in the path name of the URL

        var a = document.createElement('a');
        a.href = url;

        return  a.protocol.indexOf(CHROME) == 0 ? HTTP + ':/'
            + a.pathname.substring(PATH.length)
            + a.search
            : a.href;
    }
};