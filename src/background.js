function onPanelizerClicked(args) {
    console.log(args);

    var url = args.linkUrl
        ? [args.linkUrl, constants.analytics.actions.background.link]
        : [parser.parseUrl(args.selectionText), constants.analytics.actions.background.selection];

    panel.create(url[0]);

    analytics.track(constants.analytics.categories.background, url[1], url[0]);
}

chrome.contextMenus.create({
    "type": "normal",
    "title": "Panelizer",
    "contexts": ["link", "selection"],
    "onclick": onPanelizerClicked
});